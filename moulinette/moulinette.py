#!/usr/bin/python3
#! coding: utf-8

__doc__='''
Moulinette.

Usage:
  moulinette.py <exo_nb>
  moulinette.py (-a|--all)
  moulinette.py (-h|--help)
  moulinette.py (-v|--version)

Options:
  -h --help  Show this screen.
  -a --all  Check every exercices.
  -v --version  Show version.

'''
__title__ = "Moulinette"
__version__ = "1.0.0"
ROOT = "/home/c24b/projets/exercices/"
from docopt import docopt
import os

if __name__ == '__main__':
    arguments = docopt(__doc__, version=__version__)
    if arguments["--all"]:
        print(os.listdir(ROOT))
        exos = [str(i).zfill(3) for i in range(000, 1000) if str(i).zfill(3) in os.listdir("../../exercices/")]
        for exo in exos:
            python_f = os.path.join("./", ROOT, exo, "exo"+exo+".py")
            exec(open(python_f).read()) 
    elif arguments["--version"]:
        print(__title__+" "+ __version__)
    elif arguments['<exo_nb>']:
        print(arguments['<exo_nb>'])
    else:
        print(__doc__)
1
