# Exo001

[en] Write a programme  that prints "Hello World!"

[fr] Ecrire un programme qui affiche "Hello World!"

## Tips

[en] The programm will check the output this way:
[fr] Le programm testera l'exercice de cette manière:

```
$ python3 solution.py

```


> **Savoirs** :
* [print](https://docs.python.org/3/library/functions.html?highlight=print#print)
* [string](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)
