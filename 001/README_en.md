# Exercice 001

Write a programm that prints "Hello World!"


## Tips

The programm will check the solution this way:

```
$ python3 solution.py

```

> **Knowledge** :
* [print](https://docs.python.org/3/library/functions.html?highlight=print#print)
* [string](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)
