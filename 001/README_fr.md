# Exercice 001


Ecrire un programme qui affiche "Hello World!"

## Astuces

[fr] Le programme testera la solution de cette manière:

```
$ python3 solution.py

```

> **Savoirs** :
* [print](https://docs.python.org/3/library/functions.html?highlight=print#print)
* [string](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)
